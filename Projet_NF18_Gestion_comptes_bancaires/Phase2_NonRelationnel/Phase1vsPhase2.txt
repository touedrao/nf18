OUEDRAOGO Taoufiq
VERGNOL Calliste
BIN AHMAD Adam




## Différence UML_V3 (phase 1) et UML phase 2 :



Pour l’UML de la phase 2, nous avons appliqué le MLD en rétro-concevant notre MCD.


Nous avons donc ajouté une classe Adresse ayant comme attributs num: integer, rue: string, cp: integer d’au plus 5 chiffres et ville:string.
 La clef primaire de cette classe Adresse est une clé composée de (num, rue, cp, ville).


La classe Client n’a maintenant plus d’attribut adresse. L’information est représentée par la relation entre 
la classe Client et la classe Adresse qui est de cardinalité : Client 1..* – 1 Adresse.


Un client habite donc à une et une seule adresse. Au moins une adresse car dans l’UML_V3 de la phase 1, nous 
avions décrété que l’attribut Adresse de la classe Client était NOT NULL.




## Différence MLD_V2 (phase 1) et MLD de phase 2 :



Dans le MLD_V2, nous avions un attribut composé adresse de la relation Client :


Client(#tel : Integer, nom : String, adresse : String)


Nous avons transformé cela en :

Client(#tel : Integer, nom : String, adresse=>Adresse) 
Adresse(#num : Integer, #rue : String, #cp : Integer, #ville : String)


Ainsi, nous n’avons plus d’attribut composé. C’est la seule chose qui a changé dans notre MLD par rapport à la 
phase 1. Nous nous rendons compte que nos relations étaient assez proches des contraintes 3NF.
