OUEDRAOGO Taoufiq

VERGNOL Calliste

BIN AHMAD Adam




# ADAPTATION DE LA BDD EN NOSQL (MONGODB)


Dans cette adaptation, on décide d’imbriquer la BDD en ne gardant qu’une unique 
collection **Compte** qui contiendra toutes nos données.

**Toute les dates sont sous le format : AAAA/MM/JJ**

Voici la description de cette collection:


    Compte :
        date_crea
        statut (ouvert, fermé ou bloqué)
        balance
        solde_min
        type (epargne, revolving ou courant)
        taux
        debut_decouvert
        Proprios = un tableau de Proprio
        Operations = un tableau d’Operation



**Propio a le format suivant :**  tel, nom, adresse (cp+ville)


**Operation a le format suivant :**  montant, type (cb, emissionCheque, depotCheque, virement, retrait, depot)


**Pour faciliter les requêtes, on a retiré les attributs suivants:**

- le numéro et nom de rue pour l’adresse pour faciliter l’insertion des données.
- montant_decouvert_autorise pour le compte courant car on considère ici que tous les comptes ont un seuil minimum et donc on pourra retrouver le     
  montant de découvert en faisant le solde minimum - la balance.
- le maximum et minimum de balance pour le compte courant car on peut les retrouver facilement si on a accès à l’historique des opérations du compte.
- l’état des opérations car on considère que toutes les insertions correspondent à des opérations effectuées et donc sont déjà toutes traitées.
