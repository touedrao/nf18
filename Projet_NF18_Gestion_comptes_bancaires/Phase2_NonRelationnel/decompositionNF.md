**Il faut partir du MLD de la phase 1 et faire les décompositions nécessaires (0NF->1NF, 1NF->3NF) en justifiant ces transformations.**


OUEDRAOGO Taoufiq

VERGNOL Calliste

BIN AHMAD Adam




# DÉCOMPOSITION NF




## 0NF -> 1NF



**> Attribut multivalué : Aucun**


**> Attributs composés :**

    L’attribut <<adresse>> dans Client est composé. En effet, une adresse est composée de plusieurs domaines : Integer pour le numéro 
    de la rue, le code postal, et chaîne de caractère pour le nom de la ville et de la rue.

    Client(#tel : Integer, nom : String, adresse : String)  devient :

    Client(#tel : Integer, nom : String, adresse=>Adresse) 
    Adresse(#num : Integer, #rue : String, #cp : Integer, #ville : String)

    Et vu qu’un client fournit forcément une seule adresse et qu’une adresse peut être partagée par plusieurs clients on a une 
    cardinalité : Client 1* – 1 Adresse. 
    Si on considérait que les adresses étaient UNIQUE alors on aurait une composition 1:1 entre Client et Adresse.




## DÉCOMPOSITION 1NF —> 3NF


### 1NF :


**> Tous les attributs doivent être atomiques. C’est-à-dire non multivalué et non composé :**

    Nous considérons qu’un attribut DATE est atomique. Il n’y a aucun attribut multivalué ou composés, ils sont donc tous atomiques.


**> Il doit y avoir au moins une clef dans chaque relation :**

    Les relations Operation, CarteBleu, EmissionCheque, DepotCheque, Virement, CreditGuichet, DebitGuichet ont toutes comme clef primaire 
    un attribut id. => 1NF
    
    Les relations Compte, CompteEpargne, CompteRevolving et CompteCourant ont un attribut date_crea comme clef primaire. => 1NF

    La relation Adresse a une clé primaire composée des attributs num, rue, cp, ville. => 1NF
    La relation Asso_Compte_Client a comme clé primaire une clef composée de tel et date_crea. => 1NF


### 1NF -> 2NF

**> Doit être 1NF : Ok**

**> Il n’y a pas de partie de clef qui détermine des attributs non clé :**

    Pour toutes les tables sauf Adresse et Asso_Compte_Client toutes les clés ont un unique attribut => 2NF.

    Pour Adresse et Asso_Compte_Client, nous n’avons aucune partie de clef qui détermine un attribut non clé. => 2NF


### 2NF -> 3NF


**> Doit être 2NF : Ok**


**> Il ne doit pas y avoir d’attribut n’appartenant pas à une partie de clé qui dépende des attributs n’appartenant pas à une partie de clé :**

    Nos attributs non-clé sont :
    dans Client : nom, adresse
    dans Compte : statut
    dans CompteEpargne : balance, solde_min_const
    dans CompteRevolving : balance, montant_min
    dans CompteCourant : balance, montant_decouvert_autorise, date_debut_decouvert, max_solde, min_solde
    Operation : montant, date, etat, client, date_crea
    Asso_Compte_Client, CarteBleu, EmissionCheque, DepotCheque, Virement, CreditGuichet, DebitGuichet : aucun

    Tous les attributs non-clé ne dépendent que des clés. => 3NF


