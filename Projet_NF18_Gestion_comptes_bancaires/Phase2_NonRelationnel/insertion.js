db.Compte.insert(
{
    "date_crea": "2013/04/13",
    "balance": 2100,
    "solde_min": 0,
    "statut": "ouvert",
    "type": "courant",
    "operations":[
        {
        "montant": 25,
        "type": "virement",
        },
        {
        "montant": 43.23,
        "type": "virement",
        }
    ],
    "proprios":[
        {
        "nom": "Paul",
        "tel": 123456789,
        "adresse": {
            "cp": 67009,
            "ville": "Compinese"
            }
        },
        {
        "nom":"Mathieu",
        "tel": 323326789,
        "adresse": {
            "cp": 12302,
            "ville": "Caussoi",
            }
        }
    ]
}
)
    
db.Compte.insert(
   {
  "date_crea": "2013/04/12",
  "balance": 20300,
  "solde_min": -500,
  "statut": "ouvert",
  "type": "courant",
  "operations":[
      {
       "montant": 5000,
       "type": "dépôtchéque",
      },
{
       "montant": 300,
       "type": "débitguichet",
      }
  ],
  "proprios":[
     {
        "nom":"Sarah",
        "tel": 664803661,
        "adresse": {
           "cp": 06200,
           "ville": "Nice",
        }
     },
  ]
}
)

db.Compte.insert(
   {
  "date_crea": "2018/10/23",
  "balance": -400,
  "solde_min": -1000,
  "statut": "ouvert",
  "type": "revolving",
  "taux": 0.0001,
  "operations":[
		{
       "montant": 400,
       "type": "débitguichet",
		}
	],
  "proprios":[
     {
        "nom":"Alexis",
        "tel": 771803541,
        "adresse": {
           "cp": 60200,
           "ville": "Compiègne",
        }
     },
  ]
}
)

db.Compte.insert(
   {
  "date_crea": "2020/01/15",
  "balance": 3000,
  "solde_min": 0,
  "statut": "ouvert",
  "type": "épargne",
  "operations":[
      {
       "montant": 3000,
       "type": "virement",
      }
	],
  "proprios":[
     {
        "nom":"Sarah",
        "tel": 664803661,
        "adresse": {
           "cp": 06200,
           "ville": "Nice",
        }
     },
  ]
}
)

db.Compte.insert(
   {
  "date_crea": "2022/02/03",
  "balance": 5680,
  "solde_min": -300,
  "statut": "ouvert",
  "type": "courant",
  "operations":[
      {
       "montant": 2000,
       "type": "dépôtchèque",
      },
{
       "montant": 300,
       "type": "émissionchèque",
      }
  ],
  "proprios":[
     {
        "nom":"Lucas",
        "tel": 745431551,
        "adresse": {
           "cp": 06200,
           "ville": "Nice",
        }
     },
  ]
}
)

db.Compte.insert(
   {
  "date_crea": "2019/11/20",
  "balance": 2000,
  "solde_min": 0,
  "statut": "ouvert",
  "type": "courant",
  "operations":[
      {
       "montant": 500,
       "type": "débitguichet",
      },
{
       "montant": 200,
       "type": "débitguichet",
      }
  ],
  "proprios":[
     {
        "nom":"Ashton",
        "tel": 779832221,
        "adresse": {
           "cp": 31500,
           "ville": "Toulouse",
        }
     },
  ]
}
)

db.Compte.insert(
   {
  "date_crea": "2020/03/25",
  "balance": -200,
  "solde_min": -600,
  "statut": "ouvert",
  "type": "revolving",
  "taux": 0.0002,
  "operations":[
      {
       "montant": 200,
       "type": "créditguichet",
      },
	],
  "proprios":[
     {
        "nom":"Ashton",
        "tel": 779832221,
        "adresse": {
           "cp": 31500,
           "ville": "Toulouse",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2022/03/15",
  "balance": 320,
  "solde_min": -100,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 200,
       "type": "débitguichet",
	   },
	   {
		"montant": 500,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Clément",
        "tel": 799812525,
        "adresse": {
           "cp": 06200,
           "ville": "Nice",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2022/03/16",
  "balance": 200,
  "solde_min": 0,
  "statut": "ouvert",
  "type": "épargne",

  "operations":[
      {
       "montant": 200,
       "type": "dépôtchèque",
	   },
	],
  "proprios":[
     {
        "nom":"Clément",
        "tel": 799812525,
        "adresse": {
           "cp": 06200,
           "ville": "Nice",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2021/07/07",
  "balance": -200,
  "solde_min": -100,
  "statut": "bloqué",
  "type": "courant",

  "operations":[
      {
       "montant": 200,
       "type": "créditguichet",
	   },
	   {
		"montant": 500,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Alexandre",
        "tel": 667903355,
        "adresse": {
           "cp": 83100,
           "ville": "Toulon",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2021/09/07",
  "balance": 54300,
  "solde_min": -500,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 10000,
       "type": "dépôtchèque",
	   },
	   {
		"montant": 3400,
		"type": "émissionchèque",
	   },
	   {
		"montant": 2000,
		"type": "émissionchèque",
	   },
	   {
		"montant": 5760,
		"type": "émissionchèque",
	   },
	   
	],
  "proprios":[
     {
        "nom":"Steve",
        "tel": 667903355,
        "adresse": {
           "cp": 83000,
           "ville": "Toulon",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2020/02/06",
  "balance": 4500,
  "solde_min": -500,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 200,
       "type": "cartebleue",
	   },
	   {
		"montant": 1200,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Jean-Paul",
        "tel": 667903355,
        "adresse": {
           "cp": 83100,
           "ville": "Toulon",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2021/05/02",
  "balance": 3000,
  "solde_min": -300,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 300,
       "type": "débitguichet",
	   },
	   {
		"montant": 1000,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Maxence",
        "tel": 787404321,
        "adresse": {
           "cp": 83000,
           "ville": "Toulon",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2022/01/23",
  "balance":  1320,
  "solde_min": -100,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 300,
       "type": "cartebleue",
	   },
	   {
		"montant": 300,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Maxime",
        "tel": 655443287,
        "adresse": {
           "cp": 17000,
           "ville": "La Rochelle",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2022/04/30",
  "balance":  43324,
  "solde_min": -1000,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 3230,
       "type": "cartebleue",
	   },
	   {
		"montant": 6700,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Sophie",
        "tel": 675353287,
        "adresse": {
           "cp": 17000,
           "ville": "La Rochelle",
        }
     },
	 {
        "nom":"Benjamin",
        "tel": 675353287,
        "adresse": {
           "cp": 17000,
           "ville": "La Rochelle",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2019/10/15",
  "balance":  1320,
  "solde_min": -100,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 300,
       "type": "cartebleue",
	   },
	   {
		"montant": 300,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Maxime",
        "tel": 655443287,
        "adresse": {
           "cp": 17000,
           "ville": "La Rochelle",
        }
     },
  ]
}
)

db.Compte.insert(
{
  "date_crea": "2019/12/10",
  "balance":  2343,
  "solde_min": -300,
  "statut": "ouvert",
  "type": "courant",

  "operations":[
      {
       "montant": 200,
       "type": "cartebleue",
	   },
	   {
		"montant": 560,
		"type": "émissionchèque",
	   }
	],
  "proprios":[
     {
        "nom":"Sylvain",
        "tel": 775732210,
        "adresse": {
           "cp": 60200,
           "ville": "Compiègne",
        }
     },
  ]
}
)

