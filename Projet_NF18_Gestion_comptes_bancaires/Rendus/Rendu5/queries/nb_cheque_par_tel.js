// query.js
// compter le nombre de chèques émis par un client identifié par son numéro de téléphone.

// changer 664803661 par la valeur voulue

conn = new Mongo();
db = conn.getDB("Compte");

recordset = db.Compte.find({"proprios.tel": 664803661, "operations.type": {$in: ["dépôtchéque", "émissionchéque"]}})

var i = 0;
while ( recordset.hasNext() ) {
    recordset.next();
    i = i + 1;
}
print("Le client a émis", i, "chèque(s).");
